#include "dslrcamera.h"

#include <QDebug>
#include <QDir>

#include <stdio.h>

void error_func (GPContext *context __attribute__((unused)), const char *text, void *data __attribute__((unused))) {
 printf("Error: %s\n", text);
}

void message_func (GPContext *context __attribute__((unused)), const char *text, void *data __attribute__((unused))) {
 printf("Message: %s\n", text);
}

void DslrCamera::initCamera() {
    gp_camera_new (&this->camera);
    this->context = gp_context_new();

    // set callbacks for camera messages
    gp_context_set_error_func(this->context, error_func, NULL);
    gp_context_set_message_func(this->context, message_func, NULL);

    //gp_context_set_progress_funcs(context, progress_start, progress_update, progress_stop, NULL);

    qDebug() << "Connecting camera...\n ";
    int ret = gp_camera_init(camera, context);
    if (ret < GP_OK) {
      qDebug() << "Could not connect camera (not connected or open by gnome?).\n";
      gp_camera_free(camera);
      this->connected = false;
    } else {
      qDebug() << "Connected to camera!\n";
      this->connected = true;
    }
}

DslrCamera::DslrCamera(QObject *parent) :
    PCamera(parent)
{
    connect(this, &PCamera::updatePreview, this, &DslrCamera::updatePreviewSlot);
    connect(this, &PCamera::takePhoto, this, &DslrCamera::takePhotoSlot);

    initCamera();
}

DslrCamera::~DslrCamera()
{
    // Close gphoto2
    if (this->isConnected()) gp_camera_unref(camera);
    gp_context_unref(context);
}

void DslrCamera::updatePreviewSlot() {
    QImage image;
    const char *previewdata;
    unsigned long previewsize;
    CameraFile *file;

    if (!connected) return;

    gp_file_new(&file);
    gp_camera_capture_preview(camera, file, context);

    gp_file_get_data_and_size(file, &previewdata, &previewsize);

    image.loadFromData((const uchar*)previewdata, previewsize);

    gp_file_free(file);

    //size->setWidth(image.width());
    //size->setHeight(image.height());

    this->preview = image;

    this->previewUpdated(image);
}

void DslrCamera::takePhotoSlot(QString path) {
    QString filename = path.split("/").last();

    //gp_camera_capture(filename);
    //gp_camera_wait_for_event(camera, 10000, null, null, context);

    gp_camera_exit(camera, context);
    gp_camera_unref(camera);
    this->connected = false;

    QString command = "gphoto2 --force-overwrite --capture-image-and-download --filename ";
    command += filename;

    // Sometimes one needs to know when to be lazy ...
    if (system(command.toUtf8())) {
        photoTaken("qrc:/resources/capture_preview.jpg");
        return;
    }

    command = "mv " + filename + " " + path;

    system(command.toUtf8());

    /*QImage photo(path);

    printf("Applying frame ...\n");
    applyFrame(photo, path);
    photo.save(path);
    */

    photoTaken(path);

    initCamera();
}
