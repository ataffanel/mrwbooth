import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    width: 1024
    height: 576
    title: qsTr("Marion and Remi's Photo Booth")
    id: root
    color: "#ffffff"
    visibility: Window.Windowed

    property string backScreen: "mainMenu"

    MouseArea {
        anchors.fill: parent
        enabled: false
        cursorShape: Qt.BlankCursor
    }

    Item {
        id: group

        width: 1920
        height: 1080

        state: "welcome"

        transform: Scale { origin.x: 0; origin.y: 0; xScale:root.width/1920; yScale: root.height/1080 ; }

        /* States */
        states: [
            State {
                name: "welcome"
                PropertyChanges { target: welcome;    opacity: 1; enabled: true  }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "mainMenu"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 1; enabled: true  }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "takeScreen"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 1; enabled: true  }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },State {
                name: "takeFourPhoto"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false  }
                //PropertyChanges { target: takeFourPhoto; opacity: 1; enabled: true  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "photoView"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 1; enabled: true  }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "frameTypeChooser"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 1; enabled: true }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "frameAChooser"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 1; enabled: true  }
                PropertyChanges { target: frameBChooser; opacity: 0; enabled: false }
            },
            State {
                name: "frameBChooser"
                PropertyChanges { target: welcome;    opacity: 0; enabled: false }
                PropertyChanges { target: mainMenu;   opacity: 0; enabled: false }
                PropertyChanges { target: takeScreen; opacity: 0; enabled: false }
                //PropertyChanges { target: takeFourPhoto; opacity: 0; enabled: false  }
                PropertyChanges { target: photoView;  opacity: 0; enabled: false }
                PropertyChanges { target: frameTypeChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameAChooser; opacity: 0; enabled: false }
                PropertyChanges { target: frameBChooser; opacity: 1; enabled: true  }
            }

        ]

        /* Fonts */
        FontLoader {
            id: policetitre
            source: "resources/LilyScriptOne-Regular.ttf"
        }
        FontLoader {
            id: policetext
            source: "resources/Montserrat-ExtraLight.ttf"
        }
        FontLoader {
            id: policelogo
            source: "resources/LoversQuarrel-Regular.ttf"
        }



        /* Image used as preview */
        Image {
            id: preview
            //source: "file:///home/arnaud/capture_preview.jpg"
            source: ""
            visible: false

            Connections {
                target: camera
                onPreviewUpdated: {
                    preview.source = "image://preview/preview.jpg?" + Math.random();
                    previewTimer.running = true;
                }
            }
        }
        Timer {
            id: previewTimer
            property bool updated: true
            interval: 50;
            onTriggered: camera.updatePreview()
            repeat: false
            running: true
        }


        /* GUI */
        Welcome {
            id: welcome
            onDone: {
                group.state = "mainMenu"
            }
        }

        MainMenu {
            id: mainMenu

            onSingleSelected: {
                backScreen = "mainMenu"
                takeScreen.frame = ""
                takeScreen.mode = "simple"
                group.state = "takeScreen"
            }
            onFrameSelected: group.state = "frameTypeChooser"
            onFourSelected: {
                backScreen = "mainMenu"
                takeScreen.frame = ""
                takeScreen.mode = "four"
                group.state = "takeScreen"
            }
            onBack: group.state = "welcome"
        }

        FrameTypeChooser {
            id: frameTypeChooser
            onTypeASelected: {
                frameAChooser.base = "resources/cadres_romantique/"
                group.state = "frameAChooser"
            }
            onTypeBSelected: {
                frameAChooser.base = "resources/cadres_voyage/"
                group.state = "frameAChooser"
            }
            onBack: group.state = "mainMenu"
        }

        FrameAChooser {
            id: frameAChooser

            onBack: group.state = "frameTypeChooser"
            onFrameSelected: {
                takeScreen.frame = frameAChooser.base + frameAChooser.selected + ".png"
                takeScreen.mode = "frame"
                group.state = "takeScreen"
                backScreen = "frameAChooser"
            }
        }

        FrameBChooser {
            id: frameBChooser
        }

        TakePhotoScreen {
            id: takeScreen

            onPhotoTaken: {
                group.state = "photoView"
                photoView.source = "file:/" + tempPath + filename;
                photoView.filename = filename
                console.log(photoView.source)
            }

            onBack: group.state = backScreen
        }
/*
        TakeFourPhoto {
            id: takeFourPhoto

            onPhotoTaken: {
                group.state = "photoView"
                photoView.source = "file:/" + path;
                //photoView.frame = takeScreen.frame
                console.log(photoView.source)
            }

            onBack: group.state = backScreen
        }
*/
        PhotoView {
            id: photoView

            onDone: group.state = "welcome"
            onCancel: group.state = "takeScreen"
        }
    }
}
