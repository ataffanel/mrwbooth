import QtQuick 2.0
import QtQuick.Layouts 1.1
import "globals.js" as Globals

Rectangle {
    id: mainMenu
    anchors.fill: parent

    signal singleSelected
    signal frameSelected
    signal fourSelected
    signal back

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    Column {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 54
        }
        spacing: 50

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: Globals.fontGold + 'Choisissez le format de la photo.</font><br>' + Globals.fontSilver + 'Choose photo format.</font>';
            font.family: policetitre.name;
            font.pixelSize: 70
            color: Globals.titleColor
            horizontalAlignment: Text.AlignHCenter
        }
    }


    Item {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: 200
            bottomMargin: 250
        }
        height: 500


        GridLayout {
            anchors.fill: parent
            columns: 3
            rowSpacing: 5


            Image {
                id: simple

                Layout.maximumWidth: 500
                Layout.maximumHeight: 500/1.5
                Layout.alignment: Qt.AlignCenter

                source: preview.source

                scale: focus? 1.3:1
                z: focus?3:1
                state: focus?"selected":""

                Behavior on scale {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.OutBounce
                    }
                }

                KeyNavigation.left:four
                KeyNavigation.right: frame

                Keys.onSpacePressed: mainMenu.singleSelected()
            }
            Image {
                id: frame

                Layout.maximumWidth: 500
                Layout.maximumHeight: 500/1.5
                Layout.alignment: Qt.AlignCenter

                source: preview.source

                Image {
                    source: "resources/cadres_romantique/1.png"
                    anchors.fill:parent
                    antialiasing: true
                }

                scale: focus? 1.3:1
                z: focus?3:1
                state: focus?"selected":""

                Behavior on scale {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.OutBounce
                    }
                }

                KeyNavigation.left:simple
                KeyNavigation.right: four

                Keys.onSpacePressed: mainMenu.frameSelected()

                focus: true
            }
            FourPhoto {
                id: four

                Layout.maximumWidth: 500
                Layout.maximumHeight: 500/1.5
                Layout.alignment: Qt.AlignCenter
                source1: preview.source
                source2: preview.source
                source3: preview.source

                scale: focus? 1.3:1
                z: focus?3:1
                state: focus?"selected":""

                Behavior on scale {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.OutBounce
                    }
                }

                KeyNavigation.left:frame
                KeyNavigation.right: simple

                Keys.onSpacePressed: mainMenu.fourSelected()
            }

            Text {
                Layout.alignment: Qt.AlignCenter | Qt.AlignTop
                text: "Simple"
                font.family: policetext.name;
                font.pixelSize: 50
                color: Globals.textColor
            }
            Text {
                Layout.alignment: Qt.AlignCenter | Qt.AlignTop
                text: Globals.fontGold + "Frame</font> " + Globals.fontSilver  + "Cadre</font>"
                font.family: policetext.name;
                font.pixelSize: 50
                color: Globals.textColor
            }
            Text {
                Layout.alignment: Qt.AlignCenter | Qt.AlignTop
                text: "Photomaton"
                font.family: policetext.name;
                font.pixelSize: 50
                color: Globals.textColor
            }
        }

    }


    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        showRed: true

        showJoystick: true
    }

    Keys.onEscapePressed: back()

    onEnabledChanged: if(enabled==true) frame.focus = true
}
