#ifndef DSLRCAMERA_H
#define DSLRCAMERA_H

#include "pcamera.h"

#include <QImage>

#include <gphoto2/gphoto2.h>

class DslrCamera : public PCamera
{
    Q_OBJECT

    bool connected;

    GPContext *context;
    Camera *camera;
public:
    explicit DslrCamera(QObject *parent = 0);

    ~DslrCamera();

    //QQuickImageProvider &getImageProvider();

    bool isConnected() { return connected; }

public slots:
    void updatePreviewSlot();
    void takePhotoSlot(QString path);
    void initCamera();
};

#endif // DSLRCAMERA_H
