#include "pcamera.h"
#include <QDir>
#include <QPainter>
#include <QFile>
#include <QUrl>

class SignalImageProvider : public QQuickImageProvider {
public:
  SignalImageProvider(PCamera *camera)
          : QQuickImageProvider(QQuickImageProvider::Image)
      { this->camera = camera ;}

  //Flags flags() const { return ForceAsynchronousImageLoading; }
  QImage requestImage(const QString & id __attribute__((unused)), QSize * size __attribute__((unused)), const QSize & requestedSize __attribute__((unused)))
    Q_DECL_OVERRIDE {
      return camera->preview;
  }
private:
  PCamera *camera;
};


PCamera::PCamera(QObject *parent):
    QObject(parent)
{
   this->imageProvider = new SignalImageProvider(this);
}

void PCamera::applyFrame(QString sourcePath, QString framePath, QString destPath) {
    QImage image(sourcePath);
    QImage frameImage = QImage(QUrl(framePath).path());

    QPainter p(&image);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.drawImage(0, 0, frameImage.scaled(image.size()));
    p.end();

    image.save(destPath);
}

void PCamera::applyFourPhoto(QString source0, QString source1, QString source2,
                                    QString source3, QString destPath) {
    QImage photo0(QUrl(source0).path());
    QImage photo1(QUrl(source1).path());
    QImage photo2(QUrl(source2).path());
    QImage photo3(QUrl(source3).path());

    QSize margin(photo0.size().width()/125.0, photo0.size().height()/125.0);
    QSize size(photo0.size().width()*2 + margin.width()*3, photo0.size().height()*2 + margin.height()*3);
    QImage dest(size, photo0.format());

    QPainter p(&dest);

    p.fillRect(0, 0, size.width(), size.height(), Qt::white);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.drawImage(margin.width(), margin.height(), photo0);
    p.drawImage(margin.width()*2 + photo0.size().width(), margin.height(), photo1.scaled(photo0.size()));
    p.drawImage(margin.width(), margin.height()*2 + photo0.size().height(), photo2.scaled(photo0.size()));
    p.drawImage(margin.width()*2 + photo0.size().width(), margin.height()*2 + photo0.size().height(), photo3.scaled(photo0.size()));
    p.end();

    dest.save(destPath);
}

void PCamera::copyPhoto(QString src, QString dst) {
    QFile file(src);
    file.copy(dst);
}
