#ifndef DUMMYCAMERA_H
#define DUMMYCAMERA_H

#include <QImage>
#include "pcamera.h"

class DummyCamera : public PCamera
{
    Q_OBJECT
public:
    explicit DummyCamera(QObject *parent = 0);

    bool isConnected() { return true; }

public slots:
    void updatePreviewSlot();
    void takePhotoSlot(QString path);

};

#endif // DUMMYCAMERA_H
