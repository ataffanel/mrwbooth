import QtQuick 2.0
import QtQuick.Layouts 1.1
import "globals.js" as Globals

Rectangle {
    id: top
    anchors.fill: parent
    property string base: "resources/cadres_romantique/"
    property int selected: 1

    signal frameSelected
    signal back

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    VisualDataModel {
        id: romanticFramesModel
        model: ListModel {
            ListElement { frame: "cadres_romantique/cadre carré coeur 2.png" }
            ListElement { frame: "cadres_romantique/cadre carré coeur 2cadre jaune stylisé.png" }
        }
        delegate: FrameImage {
            width: 470
            height: width/1.5

            source: top.source
            frame: frame
        }
    }

    GridLayout {
        anchors.fill: parent
        columns: 3
        rowSpacing: 10
        anchors.leftMargin: 200
        anchors.rightMargin: 200
        anchors.topMargin: 50
        anchors.bottomMargin: 50

        FrameImage {
            id: f1

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "1.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f3
            KeyNavigation.right: f2
            KeyNavigation.up: f7
            KeyNavigation.down: f4

            Keys.onSpacePressed: {
                top.selected = 1
                top.frameSelected()
            }
        }

        FrameImage {
            id: f2

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "2.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f1
            KeyNavigation.right: f3
            KeyNavigation.up: f8
            KeyNavigation.down: f5

            Keys.onSpacePressed: {
                top.selected = 2
                top.frameSelected()
            }
        }

        FrameImage {
            id: f3

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "3.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f2
            KeyNavigation.right: f1
            KeyNavigation.up: f9
            KeyNavigation.down: f6

            Keys.onSpacePressed: {
                top.selected = 3
                top.frameSelected()
            }
        }

        FrameImage {
            id: f4

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "4.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f6
            KeyNavigation.right: f5
            KeyNavigation.up: f1
            KeyNavigation.down: f7

            Keys.onSpacePressed: {
                top.selected = 4
                top.frameSelected()
            }
        }

        FrameImage {
            id: f5

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "5.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f4
            KeyNavigation.right: f6
            KeyNavigation.up: f2
            KeyNavigation.down: f8

            Keys.onSpacePressed: {
                top.selected = 5
                top.frameSelected()
            }
        }

        FrameImage {
            id: f6

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "6.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f5
            KeyNavigation.right: f4
            KeyNavigation.up: f3
            KeyNavigation.down: f9

            Keys.onSpacePressed: {
                top.selected = 6
                top.frameSelected()
            }
        }

        FrameImage {
            id: f7

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "7.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f9
            KeyNavigation.right: f8
            KeyNavigation.up: f4
            KeyNavigation.down: f1

            Keys.onSpacePressed: {
                top.selected = 7
                top.frameSelected()
            }
        }

        FrameImage {
            id: f8

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "8.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f7
            KeyNavigation.right: f9
            KeyNavigation.up: f5
            KeyNavigation.down: f2

            Keys.onSpacePressed: {
                top.selected = 8
                top.frameSelected()
            }
        }

        FrameImage {
            id: f9

            Layout.maximumWidth: 475
            Layout.maximumHeight: 475/1.5
            Layout.alignment: Qt.AlignCenter

            source: preview.source
            frame: top.base + "9.png"

            scale: focus? 1.3:1
            z: focus?3:1
            state: focus?"selected":""

            Behavior on scale {
                NumberAnimation {
                    duration: 500
                    easing.type: Easing.OutBounce
                }
            }

            KeyNavigation.left:f8
            KeyNavigation.right: f7
            KeyNavigation.up: f6
            KeyNavigation.down: f3

            Keys.onSpacePressed: {
                top.selected = 9
                top.frameSelected()
            }
        }
    }

    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        greenFrench: ""
        greenEnglish: ""

        showRed: true
        redFrench: ""
        redEnglish: ""

        showJoystick: false
    }

    Keys.onEscapePressed: back()

    onEnabledChanged: if(enabled==true) f1.focus = true
}
