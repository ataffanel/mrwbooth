#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <QString>
#include <QImage>
#include <QQuickImageProvider>

class PCamera : public QObject
{
    Q_OBJECT

public:
    PCamera(QObject *parent = 0);

    QQuickImageProvider *getImageProvider() { return imageProvider; }
    virtual bool isConnected() { return false; }

    Q_INVOKABLE void applyFrame(QString sourcePath, QString framePath, QString destPath);
    Q_INVOKABLE void applyFourPhoto(QString source0, QString source1, QString source2,
                                    QString source3, QString destPath);
    Q_INVOKABLE void copyPhoto(QString src, QString dst);

    QImage preview;
signals:
    void previewUpdated(QImage image);
    void photoTaken(QString path);
    void updatePreview();
    void takePhoto(QString path);

private:
    QQuickImageProvider *imageProvider;
};

#endif // CAMERA_H
