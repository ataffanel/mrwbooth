import QtQuick 2.0
import "globals.js" as Globals


Item {
    property alias showRed: redButton.visible
    property alias showGreen: greenButton.visible
    property alias showJoystick: joystick.visible

    property string redFrench: "Retour"
    property string redEnglish: "Back"
    property string greenFrench: "Suivant"
    property string greenEnglish: "Next"


    Row {
        id: redButton

        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 8
        anchors.bottomMargin: 16

        spacing: 30

        Image {
            width: 120
            height: 120
            source: "resources/red.svg"
        }

        Text {
            text: "<font color='" + Globals.goldColor + "'>" + redFrench + "<br><font color='" + Globals.silverColor + "'>" + redEnglish + "</font>"
            font.family: policetitre.name;
            font.pixelSize: 40
            horizontalAlignment: Text.AlignLeft
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    Image {
        id: joystick

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 16

        source: "resources/joystick.svg"

        height: 120
        width: (120/sourceSize.height)*sourceSize.width
    }

    Row {
        id: greenButton

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 8
        anchors.bottomMargin: 16

        spacing: 30


        Text {
            text: "<font color='" + Globals.goldColor + "'>" + greenFrench + "<br><font color='" + Globals.silverColor + "'>" + greenEnglish + "</font>"
            font.family: policetitre.name;
            font.pixelSize: 40
            horizontalAlignment: Text.AlignRight
            anchors.verticalCenter: parent.verticalCenter
        }

        Image {
            width: 120
            height: 120
            source: "resources/green.svg"
        }
    }
}
