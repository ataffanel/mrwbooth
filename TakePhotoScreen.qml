import QtQuick 2.0
import "globals.js" as Globals

Rectangle {
    id: shootMenu

    signal photoTaken
    signal back

    property alias frame: img.frame

    property string mode: "simple"

    property string filename: ""

    // To take fourphoto
    property int nphoto: 0
    property string startdate: ""

    anchors.fill: parent

    opacity: 0
    enabled: false

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    Column {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 54
        }
        spacing: 20

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: mode=="four"?"Photo " + (nphoto+1) + "/3.":'Smile!';
            font.family: policetitre.name;
            font.pixelSize: 100
            color: Globals.titleColor
        }
        Text {
            text: Globals.fontGold + "La photo sera prise au second clac!</font><br>" + Globals.fontSilver + "The photo will be taken at the second click!</font>"
            font.family: policetext.name;
            font.pixelSize: 65
            horizontalAlignment: Text.AlignHCenter
            color: Globals.textColor
        }
    }

    FrameImage {
        id: img

        visible: mode=="simple" || mode=="frame"

        source: preview.source

        anchors {
            bottom: shootMenu.bottom
            left: shootMenu.left
            bottomMargin: 150
            leftMargin: 200
        }

        width: 800
        height: width/1.5
        focus: true
    }

    FourPhoto {
        id: fourImg

        visible: mode=="four"

        source1: preview.source
        source2: preview.source
        source3: preview.source

        anchors {
            bottom: shootMenu.bottom
            left: shootMenu.left
            bottomMargin: 150
            leftMargin: 200
        }

        width: 800
        height: width/1.5
        focus: true
    }

    Text {
        id: countdown
        property int count: 10
        text: count
        font.family: policetitre.name
        font.pixelSize: 400
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: Globals.goldColor

        anchors {
            bottom: shootMenu.bottom
            right: shootMenu.right
            bottomMargin: 75
            rightMargin: 75
        }

        width: 800
        height: width/1.5

    }

    Timer {
        id: countdownTimer
        repeat: true
        interval: 1000


        onTriggered: {
            countdown.count--
            if (countdown.count == 0) {
                running = false

                var date = ""

                if (mode == "four") {
                    if (nphoto == 0) {
                        date = new Date().toLocaleString(Qt.locale("en_EN"), "yyyyMMddThhmmss")
                        startdate = date
                    } else {
                        date = startdate
                    }

                    filename = date + "_" + "ff" + nphoto + ".jpg"
                    camera.takePhoto(tempPath + filename)
                } else {
                    var type = "simple"
                    if (shootMenu.frame != "") {
                        var path = String(shootMenu.frame).split("/")
                        var num = path[path.length-1].split(".")[0]
                        type = path[path.length-2].split("_")[1] + "-" + num
                    }

                    date = new Date().toLocaleString(Qt.locale("en_EN"), "yyyyMMddThhmmss")

                    shootMenu.filename =  date + "_" +  type + ".jpg"
                    camera.takePhoto(tempPath + shootMenu.filename)
                }
            }
        }
    }

    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        greenFrench: "Prendre la photo!"
        greenEnglish: "Take photo!"

        showRed: true
        showJoystick: false
    }

    Keys.onEscapePressed: if (!countdownTimer.running) back()

    Keys.onSpacePressed: countdownTimer.running = true

    onEnabledChanged: {
        if (enabled) {
            focus = true;
            countdown.count = 5;
        }
        nphoto = 0
        fourImg.source1 = Qt.binding(function() { return preview.source })
        fourImg.source2 = Qt.binding(function() { return preview.source })
        fourImg.source3 = Qt.binding(function() { return preview.source })
    }

    Connections {
        target: camera
        onPhotoTaken: {
            // Prepend final to the filename in order to keep the original photo
            if (mode == "frame") {
                camera.applyFrame(tempPath + shootMenu.filename, frame, tempPath + "final_" + filename);
            } else if (mode == "four") {
                nphoto = nphoto + 1

                if (nphoto == 1) {
                    fourImg.source1 = tempPath + filename
                    countdown.count = 5
                    countdownTimer.running = true
                    return
                } else if (nphoto == 2) {
                    fourImg.source2 = tempPath + filename
                    countdown.count = 5
                    countdownTimer.running = true
                    return
                } else if (nphoto == 3) {
                    fourImg.source3 = tempPath + filename
                    camera.applyFourPhoto(fourImg.source1, fourImg.source2, fourImg.source3, resourcePath + "/resources/four.png", tempPath + "final_" + startdate + "_ff.jpg");
                    filename = startdate + "_ff.jpg";
                }
            } else {
                camera.copyPhoto(tempPath + filename, tempPath + "final_" + filename)
            }

            filename = "final_" + filename
            photoTaken(tempPath + filename);
        }
    }
}
