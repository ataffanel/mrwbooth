import QtQuick 2.4
import QtQuick.Layouts 1.1

Rectangle {
    id: container
    property alias source1: image1.source
    property alias source2: image2.source
    property alias source3: image3.source

    width: 765
    height: 515


    Image {
        id: image1
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 5

        width: (parent.width-15)/2
        height: (parent.height-15)/2
    }
    Image {
        id: image2
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 5

        width: (parent.width-15)/2
        height: (parent.height-15)/2
    }
    Image {
        id: image3
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 5

        width: (parent.width-15)/2
        height: (parent.height-15)/2
    }
    Image {
        id: image4
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 5

        width: (parent.width-15)/2
        height: (parent.height-15)/2

        source: resourcePath + "/resources/four.png"
        mipmap: true
    }

/*
    GridLayout {
        anchors.fill: parent
        anchors.margins: 0.01*parent.width
        columns: 2

        Image {
            id: image1
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
        Image {
            id: image2
            Layout.fillWidth: true
            Layout.fillHeight: true

        }
        Image {
            id: image3
            Layout.fillWidth: true
            Layout.fillHeight: true

        }

        Image {
            Layout.fillWidth: true
            Layout.fillHeight: true

            source: resourcePath + "/resources/four.png"
            mipmap: true
        }
    }
    */
}
