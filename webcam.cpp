#include "webcam.h"
#include <QtMultimedia>
#include <QDebug>


WebCam::WebCam(QObject *parent) :
    PCamera(parent)
{
    connect(this, &PCamera::takePhoto, this, &WebCam::takePhotoSlot);

    if (QCameraInfo::availableCameras().count() == 0) {
        connected = false;
        return;
    }

    camera = new QCamera(QCameraInfo::availableCameras().first());
    camera->setViewfinder(new MyVideoSurface(this));

    camera->start();

    connected = true;
}

WebCam::~WebCam()
{
    if (connected) delete camera;
}

void WebCam::takePhotoSlot(QString path)
{
    //applyFrame(preview, path);
    preview.save(path);
    photoTaken(path);
}

bool MyVideoSurface::present(const QVideoFrame &frame)
{
    QVideoFrame *f = (QVideoFrame*)&frame;

    if (f->map(QAbstractVideoBuffer::ReadOnly)) {
        QImage image = QImage(frame.bits(), frame.width(), frame.height(), frame.bytesPerLine(), frame.imageFormatFromPixelFormat(frame.pixelFormat()));
        //memcpy(image.bits(), frame.bits(), frame.bytesPerLine()*frame.height());

        int newWidth = frame.height()*1.5;
        QRect rect = QRect((frame.width()-newWidth)/2, 0, newWidth, frame.height());

        camera->preview = image.copy(rect);
        camera->previewUpdated(image);
    }

    return true;
}

QList<QVideoFrame::PixelFormat> MyVideoSurface::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const
{
    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB24;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}
