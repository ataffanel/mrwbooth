import QtQuick 2.0

Item {
    id: container
    property alias src: shader.src
    property alias color: shader.edgeColor

    ShaderEffect {
        id: shader
        property variant src: preview
        property size unit : Qt.size(1/width, 1/height)
        property color edgeColor: "lime"

        //width: preview.width; height: preview.height

        //anchors.left: preview.right
        //anchors.top: preview.top

        anchors.fill: parent

        fragmentShader:  "
        varying highp vec2 qt_TexCoord0;
        uniform sampler2D src;
        uniform lowp float qt_Opacity;
        uniform mediump vec2 unit;
        uniform lowp vec4 edgeColor;

        /* void main() {
            lowp vec4 tex = texture2D(src, vec2(qt_TexCoord0.x/2.0, qt_TexCoord0.y));
            gl_FragColor = vec4(vec3(dot(tex.rgb, vec3(0.344, 0.5, 0.156))), tex.a) * qt_Opacity;
        */
        void main(void) {
            float x = qt_TexCoord0.x;
            float y = qt_TexCoord0.y;
            float x1 = x - unit.x;
            float y1 = y - unit.y;
            float x2 = x + unit.x;
            float y2 = y + unit.y;
            vec4 p0 = texture2D(src, vec2(x1, y1));
            vec4 p1 = texture2D(src, vec2(x, y1));
            vec4 p2 = texture2D(src, vec2(x2, y1));
            vec4 p3 = texture2D(src, vec2(x1, y));
            /* vec4 p4 = texture2D(src, vec2(x, y)); */
            vec4 p5 = texture2D(src, vec2(x2, y));
            vec4 p6 = texture2D(src, vec2(x1, y2));
            vec4 p7 = texture2D(src, vec2(x, y2));
            vec4 p8 = texture2D(src, vec2(x2, y2));

            vec4 v =  p0 + (2.0 * p1) + p3 -p6 + (-2.0 * p7) + -p8;
            vec4 h =  p0 + (2.0 * p3) + p7 -p2 + (-2.0 * p5) + -p8;
            //gl_FragColor = sqrt(h*h + v*v);
            //gl_FragColor = (h*h + v*v);

            if (length((h*h + v*v))>0.99) {
                gl_FragColor = edgeColor;
            } else {
                 gl_FragColor = texture2D(src, vec2(x, y));
            }


            //gl_FragColor.a = 1.0;
        }"
    }
}
