#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQuickImageProvider>
#include <QThread>
#include <QQmlContext>
#include <QObject>
#include <QDir>

#include <QJsonDocument>
#include <QJsonObject>


#include <gphoto2/gphoto2.h>

#include "pcamera.h"
#include "dslrcamera.h"
#include "dummycamera.h"
#include "webcam.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    PCamera *camera;

    qDebug() << "Trying dslr ...\n";
    camera = new DslrCamera();
    if (!camera->isConnected()) {
        delete camera;
        qDebug() << "Trying webcam ...\n";
        camera = new WebCam();
        if (!camera->isConnected()) {
            delete camera;
            qDebug() << "Trying dummy ...\n";
            camera = new DummyCamera();
        }
    }

//    QFile settingsFile("settings.json");
//    QJsonDocument settingsDocument = QJsonDocument::fromJson(settingsFile.readAll());
//    QJsonObject settings = settingsDocument.object();

//    qWarning() << settings["hello"].toString();

    QString photoPath = QDir::currentPath() + "/../booth_final/";
    QString tempPath = QDir::currentPath() + "/../booth_temp/";
    QString resourcePath = QDir::currentPath() + "/../mrwbooth/";

    QQmlApplicationEngine engine;
    engine.addImageProvider("preview", camera->getImageProvider());
    engine.rootContext()->setContextProperty("camera", camera);
    engine.rootContext()->setContextProperty("photoPath", photoPath);
    engine.rootContext()->setContextProperty("tempPath", tempPath);
    engine.rootContext()->setContextProperty("resourcePath", resourcePath);
    engine.load(QUrl(resourcePath + QStringLiteral("main.qml")));

    return app.exec();
}
