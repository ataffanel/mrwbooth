import QtQuick 2.0
import QtQuick.Layouts 1.1
import "globals.js" as Globals

Rectangle {
    id: top
    anchors.fill: parent

    signal typeASelected
    signal typeBSelected
    signal back

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    Column {
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 54
        }
        spacing: 50

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: Globals.fontGold + 'Choisissez le type de cadre.</font><br>' + Globals.fontSilver + 'Choose frame type.</font>'
            font.family: policetitre.name;
            font.pixelSize: 70
            color: Globals.titleColor
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Item {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: 200
            bottomMargin: 250
        }
        height: 500


        GridLayout {
            anchors.fill: parent
            columns: 2
            rowSpacing: 60


            FrameImage {
                id: typeA
                focus: true

                Layout.maximumWidth: 500
                Layout.maximumHeight: 500/1.5
                Layout.alignment: Qt.AlignCenter

                source: preview.source
                frame: "resources/cadres_romantique/1.png"

                scale: focus? 1.3:1
                z: focus?3:1
                state: focus?"selected":""

                Behavior on scale {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.OutBounce
                    }
                }

                KeyNavigation.left:typeB
                KeyNavigation.right: typeB

                Keys.onSpacePressed: top.typeASelected()
            }
            Image {
                id: typeB

                Layout.maximumWidth: 500
                Layout.maximumHeight: 500/1.5
                Layout.alignment: Qt.AlignCenter

                source: preview.source

                Image {
                    source: "resources/cadres_voyage/6.png"
                    anchors.fill:parent
                    antialiasing: true
                }

                scale: focus? 1.3:1
                z: focus?3:1
                state: focus?"selected":""

                Behavior on scale {
                    NumberAnimation {
                        duration: 500
                        easing.type: Easing.OutBounce
                    }
                }

                KeyNavigation.left:typeA
                KeyNavigation.right: typeA

                Keys.onSpacePressed: top.typeBSelected()
            }

            Text {
                Layout.alignment: Qt.AlignCenter | Qt.AlignTop
                text: "Simples"
                font.family: policetitre.name;
                font.pixelSize: 50
                color: Globals.textColor
            }
            Text {
                Layout.alignment: Qt.AlignCenter | Qt.AlignTop
                text: "Theme"
                font.family: policetitre.name;
                font.pixelSize: 50
                color: Globals.textColor
            }

        }

    }


    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        greenFrench: "Suivant"
        greenEnglish: "Next"

        showRed: true
        redFrench: "Retour"
        redEnglish: "back"

        showJoystick: true
    }

    Keys.onEscapePressed: back()

    onEnabledChanged: if(enabled==true) typeA.focus = true
}
