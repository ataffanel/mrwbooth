import QtQuick 2.4
import "globals.js" as Globals

Rectangle {
    anchors.fill: parent

    signal done;
    signal cancel;

    property string filename: ""
    property alias source: image.source

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    Text {
        text: Globals.fontGold + "Accessible lundi sur </font>&nbsp;&nbsp;&nbsp;" + Globals.fontSilver + " Available Monday on</font><br>www.arnaudetemilie.fr"

        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 35
        font.family: policetitre.name;
        font.pixelSize: 70
        color: Globals.titleColor
        horizontalAlignment: Text.AlignHCenter

    }

    Image {
        id: image

        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.leftMargin: 100

        height: 750
        width: height*1.5

        mipmap: true
    }

    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        greenFrench: "Enregistrer"
        greenEnglish: "Save"

        showRed: true
        redFrench: "Reprendre"
        redEnglish: "Take again"

        showJoystick: false
    }

    onEnabledChanged: {
        if (enabled) {
            focus = true;
        }
    }

    Keys.onSpacePressed: {
        camera.copyPhoto(tempPath + filename, photoPath + filename)
        done()
    }
    Keys.onEscapePressed: cancel()
}
