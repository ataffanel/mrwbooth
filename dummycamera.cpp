#include "dummycamera.h"

#include <qdebug.h>

DummyCamera::DummyCamera(QObject *parent) :
    PCamera(parent)
{
    connect(this, &PCamera::updatePreview, this, &DummyCamera::updatePreviewSlot);
    connect(this, &PCamera::takePhoto, this, &DummyCamera::takePhotoSlot);
    preview = QImage();
}

void DummyCamera::updatePreviewSlot() {
    preview.load("../mrwbooth/resources/capture_preview.jpg");
    previewUpdated(preview);
}

void DummyCamera::takePhotoSlot(QString path) {
    QImage photo = QImage("../mrwbooth/resources/capture_preview.jpg");
    photo.save(path);
    photoTaken(path);
}
