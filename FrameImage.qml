import QtQuick 2.0

Image {
    property alias frame: frame.source

    Image {
        id: frame

        anchors.fill:parent
        antialiasing: true
    }
}
