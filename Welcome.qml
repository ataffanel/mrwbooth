import QtQuick 2.0
import QtQuick.Window 2.0
import "globals.js" as Globals

Rectangle {
    anchors.fill: parent

    width: 1920
    height: 1080

    signal done;

    color: "#ffffff"

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }

    Text {
        // Format doc: https://doc.qt.io/qt-5/qml-qtquick-text.html#textFormat-prop
        text: "<font color='" + Globals.goldColor + "'>Bienvenue sur</font> &nbsp;&nbsp;&nbsp; <font color='" + Globals.silverColor + "'>Welcome on</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br><font size='4'>Emilie &amp; Arnaud<br>PhotoBooth<font>"

        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 50
        font.family: policetitre.name;
        font.pixelSize: 70
        color: Globals.titleColor
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: text1
        text: Globals.fontGold + "Les photos prises seront accessibles dès lundi sur:</font><br>" + Globals.fontSilver + "Pictures are going to be available Monday on:</font>"

        wrapMode: Text.WordWrap
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 450
        font.family: policetext.name;
        font.pixelSize: 50
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: text2
        text: "www." + Globals.fontGold + "arnaud</font>et" + Globals.fontSilver + "emilie</font>.fr"

        wrapMode: Text.WordWrap
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 600
        font.family: policetitre.name;
        font.pixelSize: 100
        color: Globals.titleColor
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: text3
        text: "AE"

        x: 867
        y: 887
        anchors.right: parent.right
        anchors.rightMargin: 867
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 25

        font.pixelSize: 150
        font.family: policelogo.name
    }

    Navigation {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        showGreen: true
        showRed: false
        showJoystick: false
    }

    onEnabledChanged: {
        if (enabled) {
            focus = true;
        }
    }
    focus: true

    Keys.onSpacePressed: done()

    Keys.onReleased: {
        if(event.key == Qt.Key_F) {
            if(root.visibility == Window.FullScreen) {
                root.visibility = Window.Windowed;
            } else {
                root.visibility = Window.FullScreen;
            }
        }
    }
}
