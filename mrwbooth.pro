TEMPLATE = app

QT += qml quick widgets multimedia

SOURCES += main.cpp \
    pcamera.cpp \
    dslrcamera.cpp \
    dummycamera.cpp \
    webcam.cpp

RESOURCES +=

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    pcamera.h \
    dslrcamera.h \
    dummycamera.h \
    webcam.h

unix|win32: LIBS += -lgphoto2

DISTFILES += \
    globals.js \
    Navigation.qml

OTHER_FILES += \
    FourPhoto.qml \
    FrameAChooser.qml \
    FrameBChooser.qml \
    FrameImage.qml \
    FrameTypeChooser.qml \
    MainMenu.qml \
    PhotoView.qml \
    TakePhotoScreen.qml \
    main.qml \
    Welcome.qml
