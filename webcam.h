#include <QObject>
#include <QtMultimedia>
#include <pcamera.h>

#ifndef WEBCAM_H
#define WEBCAM_H

class WebCam : public PCamera
{
    Q_OBJECT

    bool connected;
    QCamera *camera;

public:
    explicit WebCam(QObject *parent = 0);
    ~WebCam();

    bool isConnected() { return connected; }

signals:

public slots:
    void takePhotoSlot(QString path);

};

class MyVideoSurface : public QAbstractVideoSurface
{
    Q_OBJECT
    PCamera *camera;

public:
    MyVideoSurface(PCamera *camera, QObject *parent = 0): QAbstractVideoSurface(parent)
    {
        this->camera = camera;
    }

    bool present(const QVideoFrame &frame);
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;
};

#endif // WEBCAM_H
