import QtQuick 2.0

Item {
    anchors.fill: parent

    signal type1Selected
    signal type2Selected

    Behavior on opacity {
        NumberAnimation {
            duration: 200
            easing.type: Easing.OutCurve
        }
    }
}
